package com.hcl.springbooth2.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.hcl.springbooth2.entity.Student;

public interface IStudentService {
	
	
	public Student  addStudent(Student student);
	
	public List<Student> getAllStudent();
	
	public Student updateStudent(Student student);
	
	public  ResponseEntity<String>  deleteById(Long id);
	
	
}
