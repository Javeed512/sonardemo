package com.hcl.springbooth2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hcl.springbooth2.entity.Student;
import com.hcl.springbooth2.repository.StudentRepository;

@Service
public class StudentServiceImp  implements  IStudentService  {

		@Autowired
		StudentRepository repo;
	
	
	@Override
	public Student addStudent(Student student) {
		// TODO Auto-generated method stub
		return   repo.save(student);
	}


	@Override
	public List<Student> getAllStudent() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}


	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return repo.save(student);
	}


	@Override
	public ResponseEntity<String> deleteById(Long id) {
		// TODO Auto-generated method stub
				repo.deleteById(id);
				
			return new ResponseEntity<String>("Record deleted",HttpStatus.ACCEPTED );	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
