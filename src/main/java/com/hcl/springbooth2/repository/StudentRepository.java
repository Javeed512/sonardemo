package com.hcl.springbooth2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.springbooth2.entity.Student;
@Repository
public interface StudentRepository  extends JpaRepository<Student, Long>  {

	
	
}
